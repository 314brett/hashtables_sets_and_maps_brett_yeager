/**
 * Brett Yeager
 * This class tests the HashTable, Set and Map
 * UnitTester.java
 * 3/09/2018
 */
package UnitTester;

import HashTable.HashTable;
import Maps.Map;
import Sets.Set;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * This class tests the HashTable, Set and Map
 * @author Brett Yeager
 * @version 1
 */
public class UnitTester {
    HashTable<Integer> hashTable;
    Set<Integer> set;
    Map<Integer, Integer> map;

    @Before
    public void beforeTests(){
        hashTable = new HashTable<>();
        set = new Set<>();
        map = new Map<>();
    }

    @Test
    public void testAdd(){
        hashTable.add(1);
        Assert.assertTrue("Failed to add to hashTable", hashTable.contains(1));
        hashTable.add(1);
        hashTable.add(1);
        Assert.assertTrue("Table should not add duplicates",
                hashTable.size()==1);
        hashTable.add(11);
        Assert.assertTrue("Table failed to handle collision",
                hashTable.contains(11));

        for(int i=2; i<5000; i++){
            hashTable.add(i);
        }

        boolean containsAllOfThese = true;
        for(int i=1; i<5000; i++){
            if(!hashTable.contains(i)){
                containsAllOfThese = false;
            }
        }
        Assert.assertTrue("Rehashing hashTable failed. Not all elements were" +
                " moved into the new hashTable.", containsAllOfThese);

    }

    @Test
    public void testRemove(){
        hashTable.add(5);
        hashTable.remove(5);

        Assert.assertFalse("Failed to remove element", (hashTable.contains(5)));

        hashTable.add(5);
        hashTable.add(15);
        hashTable.remove(15);

        Assert.assertTrue("Failed to remove second element at index",
                (!hashTable.contains(15)));

        try{
            hashTable.remove(8);

            Assert.fail("Removing an element not in the hashTable should throw " +
                    "a NoSuchElementException");
        }catch(NoSuchElementException ex){
            //do nothing
        }

    }

    @Test
    public void testContains(){
        hashTable.add(25);
        hashTable.add(35);
        Assert.assertTrue("Contains method doesn't find the element",
                hashTable.contains(25));
        Assert.assertTrue("Contains method doesn't find the second element" +
                        " in an index",
                hashTable.contains(35));
    }

    @Test
    public void testSize(){
        Assert.assertTrue("Table size should start at zero", hashTable.size()==0);
        for(int i=0; i<10; i++){
            hashTable.add(i);
        }
        Assert.assertTrue("Size method failed to return correct value " +
                        "after adding elements",
                hashTable.size()==10);
        for(int i=10; i<500; i++){
            hashTable.add(i);
        }
        Assert.assertTrue("Size method failed to return correct value " +
                        "after adding lots of values",
                hashTable.size()==500);

    }

    @Test
    public void testIsEmpty(){
        Assert.assertTrue("Table should start empty", hashTable.isEmpty());
        hashTable.add(5);
        Assert.assertFalse("isEmpty() should return false when data " +
                        "is in the hashTable",
                hashTable.isEmpty());
        hashTable.remove(5);
        Assert.assertTrue("isEmpty() should return true after all " +
                        "of the data is removed",
                hashTable.isEmpty());
    }

    @Test
    public void testClear(){
        hashTable.add(2);
        hashTable.add(3);
        hashTable.add(4);
        int sizeBeforeClear = hashTable.size();
        hashTable.clear();
        Assert.assertTrue("Clearing hashTable failed",
                hashTable.size()==0 && sizeBeforeClear==3);
    }

    @Test
    public void testGet(){
        Object nullElement = hashTable.get(3);
        Assert.assertNull("Table should return null if it can't " +
                "find the object.", nullElement);

        Integer element = 1;
        hashTable.add(element);
        Object returnedElement = (Object) hashTable.get(element);
        Assert.assertTrue("Get doesn't return the same object that was added",
                returnedElement==element);
    }

    @Test
    public void testIterator(){
        int[] testIntegers = {4, 14, 24, 1, 11, 51};
        for(int testInt : testIntegers){
            hashTable.add(testInt);
        }

        List allElementsInTable = new LinkedList();
        for(Object tableObj : hashTable){
            int tableInt = (int) tableObj;
            allElementsInTable.add(tableInt);
        }

        boolean foundAllElements = true;

        for(int testInt : testIntegers){
            if(!allElementsInTable.contains(testInt)){
                foundAllElements = false;
            }
        }

        Assert.assertTrue("Iterator failed to return all of the elements",
                foundAllElements);

    }

    @Test
    public void testSetUnion(){
        Set<Integer> otherSet = new Set<Integer>();
        set.add(1);
        set.add(2);
        set.add(3);

        otherSet.add(4);
        otherSet.add(5);
        otherSet.add(6);

        set = (Set) set.union(otherSet);


        Assert.assertTrue("Union did not returned a set of all of the values",
                set.contains(1) && set.contains(2) && set.contains(3) &&
                set.contains(4) && set.contains(5) && set.contains(6));
    }

    @Test
    public void testSetDifference(){
        Set<Integer> otherSet = new Set<Integer>();
        set.add(1);
        set.add(2);
        set.add(3);

        otherSet.add(2);
        otherSet.add(3);
        otherSet.add(4);

        set = (Set) set.difference(otherSet);


        Assert.assertTrue("Too many items were not removed after difference()"
                , set.contains(1));
        Assert.assertFalse("Item was not removed after calling difference()"
                , set.contains(3));
    }

    @Test
    public void testMapAdd(){
        map.add(3, 5);
        Assert.assertTrue("Map did not add value",(Integer) map.get(3)==5);
    }
    @Test
    public void testMapKeyExists(){
        map.add(3, 5);
        Assert.assertTrue("Map did not find key", map.keyExists(3));
        Assert.assertFalse("keyExists should look for keys, not values",
                map.keyExists(5));
    }
    @Test
    public void testMapValueExists(){
        map.add(3, 5);
        Assert.assertTrue("Map didn't find a value", map.valueExists(5));
        Assert.assertFalse("valueExists should look for values, not keys",
                map.valueExists(3));
    }
    @Test
    public void testMapSize(){
        Assert.assertTrue("Map should start with size 0", map.size()==0);
        map.add(3, 5);
        Assert.assertTrue("map size incorrect after adding element",
                map.size()==1);
    }
    @Test
    public void testMapEmpty(){
        Assert.assertTrue("Map should start empty", map.isEmpty());
        map.add(3, 5);
        Assert.assertFalse("Map should not be empty after adding element",
                map.isEmpty());
        map.clear();
        Assert.assertTrue("Map should be empty after clearing map", map.isEmpty());
    }
    @Test
    public void testMapIterator(){
        map.add(0, 1);
        map.add(1, 2);
        map.add(2, 3);
        int sum = 0;
        for(Object keyValuePair : map){
            Map.KeyValuePair pair = (Map.KeyValuePair) keyValuePair;
            sum += (Integer) pair.value;
        }
        Assert.assertTrue("Iterator did not return correct elements", sum==6);
    }

}
