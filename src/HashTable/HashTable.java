/**
 * Brett Yeager
 * This class is a Hash Table
 * HashTable.java
 * 3/09/2018
 */
package HashTable;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This class is a hash table that uses separate chaining
 * @author Brett Yeager
 * @version 1
 */
public class HashTable<T> implements ICollection {

    Node<T>[] table;
    public static final int INITIAL_SIZE = 10;
    private int size;
    public static final double MAX_LOAD_FACTOR = 2.5;
    public static final double INCREASE_FACTOR = 1.5;

    /**
     * Initialize the hash table
     */
    public HashTable(){
        table = new Node[INITIAL_SIZE];
        putLinkedListsInTable();
    }

    //A linked list is stored at each position in the table
    private void putLinkedListsInTable(){
        for(int i=0; i<table.length; i++){
            table[i] = new Node<T>();
        }
    }


    /**
     * Adds an element to the collection. No specific ordering
     * is required.
     *
     * @param element the new element to put in the collection
     */
    @Override
    public void add(Object element) {
        //If we already have the element, don't add it
        if(contains(element)){
            return;
        }
        int position = element.hashCode()%table.length;
        if(table[position].data==null){
            //We can add the data to the top node
            table[position].data = (T) element;
            size += 1;
            return;
        }

        //We need to add a new node to the list
        Node parent = table[position];
        while(parent.next!=null){
            parent = parent.next;
        }
        Node newNode = new Node();
        newNode.data = element;
        parent.next = newNode;
        size += 1;

        if(getLoadFactor()>MAX_LOAD_FACTOR){
            rehash();
        }
    }

    private void rehash(){
        Node[] oldTable = table;
        table = new Node[(int) (table.length * INCREASE_FACTOR)];
        size = 0;
        putLinkedListsInTable();

        for(Node node : oldTable){
            while(node!=null){
                if(node.data!=null){
                    add(node.data);
                }
                node = node.next;
            }
        }

    }
    private double getLoadFactor(){
        return size/(double) table.length;
    }

    /**
     * Finds and removes an element from the collection.
     *
     * @param element the element to remove
     * @throws NoSuchElementException thrown when the
     *                                element is not found in the collection
     */
    @Override
    public void remove(Object element) {
        int position = element.hashCode()%table.length;

        //Special case, the element is at the top of the list
        if(table[position].data!=null && table[position].data.equals(element)){
            if(table[position].next==null) {
                table[position].data = null;
            }else{
                table[position] = table[position].next;
            }
            size -= 1;
            return;
        }

        //Otherwise, search the list for the element
        Node currentElement = table[position];
        while(currentElement.next!=null){
            if(currentElement.next.data.equals(element)){
                currentElement.next = currentElement.next.next;
                size -= 1;
                return;
            }
            currentElement = currentElement.next;
        }

        //We didn't find it
        throw new NoSuchElementException("Could not remove: " +
                "element was not found");

    }

    /**
     * Reports whether the collection contains an element.
     *
     * @param element the element to search for.
     * @return true if the element is found, otherwise false
     */
    @Override
    public boolean contains(Object element) {
        for(Object item : this){
            if(item.equals(element)){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the number of elements in the collection.
     *
     * @return the number of elements
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Reports whether the collection is empty or not.
     *
     * @return true if the collection is empty, otherwise false
     */
    @Override
    public boolean isEmpty() {
        return size==0;
    }

    /**
     * Removes all elements from the collection.
     */
    @Override
    public void clear() {
        size = 0;
        table = new Node[INITIAL_SIZE];
    }

    /**
     * Returns an element in the collection that matches the
     * input parameter according the equals() method of the
     * parameter.
     *
     * @param element an element to search for
     * @return a matching element
     */
    @Override
    public Object get(Object element) {

        for(Object item : this){
            if(item.equals(element)){
                return item;
            }
        }
        return null;
    }

    /**
     * Returns an iterator over the collection.
     *
     * @return an object using the Iterator<T> interface
     */
    @Override
    public Iterator iterator() {
        return new NodeIterator(table);
    }

    /**
     * This class is an iterator
     */
    private class NodeIterator implements Iterator {

        int currentPosition;//Index in table
        Node currentNode;//Current node in a linked list
        Node[] table;

        /**
         * Instantiate the iterator
         */
        public NodeIterator(Node[] table){
            this.table = table;
            currentPosition = 0;
            currentNode = table[0];
        }

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            if(currentNode==null || currentNode.data==null){
                //Only go to the next one if the currentNode is null
                goToNext();
            }
            return currentNode!=null;
        }

        private void goToNext(){
            //If the node has a next item, go to next
            if(currentNode!=null && currentNode.next!=null){
                currentNode = currentNode.next;
                System.out.println("found one");
                return;
            }

            //Otherwise, move down the list
            do{
                currentPosition += 1;
                if(currentPosition==table.length){
                    //We reached the end of the table
                    currentNode = null;
                    return;
                }
                currentNode = table[currentPosition];
            }while(table[currentPosition].data==null);

            //We found a position with a node
            currentNode = table[currentPosition];
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iteration has no more elements
         */
        @Override
        public T next() {
            hasNext();
            Node tempNode = currentNode;
            currentNode = currentNode.next;
            return (T) tempNode.data;
        }
    }

    /**
     * This class is a node that forms the linked lists in the table
     */
    private class Node<T>{
        public T data;
        public Node next;

    }
}
