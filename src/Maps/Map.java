/**
 * Brett Yeager
 * This class is a map
 * Map.java
 * 3/16/2018
 */
package Maps;

import HashTable.HashTable;
import HashTable.ICollection;
import Sets.ISet;
import Sets.Set;

import java.util.Iterator;
import java.util.Objects;

/**
 * This class is a map
 * @author Brett Yeager
 * @version 1
 * @param <K> Key
 * @param <V> Value
 */
public class Map<K, V> implements IMap {

    HashTable<KeyValuePair> table;

    public Map(){
        table = new HashTable<>();
    }

    /**
     * Adds a key/value pair to the map. If the key already exists
     * in the map then this will update the value associated with
     * the key.
     *
     * @param key   the key
     * @param value the value
     */
    @Override
    public void add(Object key, Object value) {
        KeyValuePair<K, V> pair = new KeyValuePair();
        pair.key = (K) key;
        pair.value = (V) value;

        //Replace the element if it's already in the table
        if(table.contains(pair)){
            table.remove(pair);
        }
        table.add(pair);

    }

    /**
     * Returns the value associated with a key.
     *
     * @param key the key
     * @return the value associated with the key
     */
    @Override
    public Object get(Object key) {

        KeyValuePair<K, V> pair = new KeyValuePair();
        pair.key = (K) key;

        //Return the value from the key value pair that was returned
        return ((KeyValuePair) table.get(pair)).value;
    }

    /**
     * Reports whether the input key is in the map.
     *
     * @param key the key
     * @return true if the key is in the map, otherwise false
     */
    @Override
    public boolean keyExists(Object key) {

        KeyValuePair<K, V> pair = new KeyValuePair();
        pair.key = (K) key;

        //Check if the table has the key
        return table.contains(pair);
    }

    /**
     * Reports whether the input value is in the map.
     *
     * @param value the value
     * @return true if the value is in the map, otherwise false
     */
    @Override
    public boolean valueExists(Object value) {
        for(Object obj : table){
            KeyValuePair pair = (KeyValuePair) obj;
            if(pair.value==value){
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the number of key/value pairs in the map.
     *
     * @return the number of elements
     */
    @Override
    public int size() {
        return table.size();
    }

    /**
     * Reports whether the map is empty or not.
     *
     * @return true if no key/value pairs are in the map, otherwise
     * false
     */
    @Override
    public boolean isEmpty() {
        return table.isEmpty();
    }

    /**
     * Removes all key/value pairs in the map.
     */
    @Override
    public void clear() {
        table.clear();
    }

    /**
     * Returns an iterator over the key/value pairs in the map.
     *
     * @return an iterator using the Iterator<T> interface.
     */
    @Override
    public Iterator<Map.KeyValuePair> iterator() {
        return table.iterator();
    }

    /**
     * Returns an ISet<K> object with the all keys in the map.
     *
     * @return a set of keys
     */
    @Override
    public ISet keyset() {
        Set keys = new Set();
        for(Object obj : table){
            KeyValuePair pair = (KeyValuePair) obj;
            keys.add(pair.key);
        }
        return keys;
    }

    /**
     * Returns an ICollection<V> object with all values in the map.
     *
     * @return a collection of values
     */
    @Override
    public ICollection values() {
        HashTable values = new HashTable();
        for(Object obj : table){
            KeyValuePair pair = (KeyValuePair) obj;
            values.add(pair.value);
        }
        return values;
    }


    public class KeyValuePair<K, V>
    {
        public K key;
        public V value;

        @Override
        public boolean equals(Object other) {
            //If they are the same class
            if(getClass() == other.getClass()){
                KeyValuePair otherPair  = (KeyValuePair) other;
                //And have an equal key, they are equal
                if(key.equals(otherPair.key)){
                     return true;
                }
            }
            return false;
        }

        @Override
        public int hashCode() {

            return Objects.hash(key);
        }
    }

}
