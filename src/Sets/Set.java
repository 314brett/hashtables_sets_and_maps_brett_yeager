/**
 * Brett Yeager
 * This class is a set
 * Set.java
 * 3/16/2018
 */
package Sets;

import HashTable.HashTable;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This class is a set
 * @author Brett Yeager
 * @version 1
 * @param <T> Value type
 */
public class Set<T> implements ISet<T> {

    HashTable<T> table;

    public Set(){
        table = new HashTable<T>();

    }

    /**
     * Returns a new set object with the union of
     * the current set and the input parameter.
     *
     * @param other the other set to join with this set
     * @return the union of two sets
     */
    @Override
    public ISet<T> union(ISet<T> other) {
        Set<T> newSet = new Set<T>();

        //Put all of the values in the new set
        for(Object value : table){
            newSet.add((T) value);
        }
        for(Object otherValue : other){
            newSet.add((T) otherValue);
        }
        return newSet;
    }

    /**
     * Returns a new set object with the intersection
     * of the current set and the input parameter.
     *
     * @param other the other set to join with this set
     * @return a intersection of two sets
     */
    @Override
    public ISet<T> intersects(ISet<T> other) {
        Set<T> newSet = new Set<T>();

        //Add each value to the new set if both sets have that value
        for(Object value : table){
            if(other.contains((T) value))
            newSet.add((T) value);
        }
        return newSet;
    }

    /**
     * Returns a new set object with the difference
     * between this set and the input set.
     * (i.e. this - other)
     *
     * @param other the other set to use in the difference
     *              operation
     * @return a difference of two sets
     */
    @Override
    public ISet<T> difference(ISet<T> other) {
        Set<T> newSet = new Set<T>();

        //Put each value in the new set if the other set doesn't have it
        Set[] sets = {this, (Set) other};//Store both sets

        //For each set
        for(int aSet=0; aSet<=1; aSet++){
            int otherSet = 1-aSet;

            //If the other set doesn't contain the value, add it to the new set
            for(Object value : sets[aSet]){
                if(!sets[otherSet].contains((T) value))
                    newSet.add((T) value);
            }
        }
        return newSet;
    }

    /**
     * Reports whether the input set is a subset of this
     * set.
     *
     * @param other the subset candidate
     * @return true if other is a subset of this set,
     * otherwise false
     */
    @Override
    public boolean isSubset(ISet<T> other) {
        for(T data : other){
            if(!contains(data)){
                return false;
            }
        }

        return true;
    }

    /**
     * Reports whether this set and the input set are
     * disjoint.
     *
     * @param other the other set to consider
     * @return true if both sets are disjoint, otherwise
     * false
     */
    @Override
    public boolean isDisjoint(ISet<T> other) {

        //If they have data in common, they are not disjoint
        for(T data : other){
            if(contains(data)){
                return false;
            }
        }

        return true;
    }

    /**
     * Reports whether this set is an empty set.
     *
     * @return true if this set is empty, otherwise false
     */
    @Override
    public boolean isEmptySet() {
        return table.isEmpty();
    }

    /**
     * Adds an element to the collection. No specific ordering
     * is required.
     *
     * @param element the new element to put in the collection
     */
    @Override
    public void add(T element) {
        table.add(element);
    }

    /**
     * Finds and removes an element from the collection.
     *
     * @param element the element to remove
     * @throws NoSuchElementException thrown when the
     *                                element is not found in the collection
     */
    @Override
    public void remove(T element) {
        table.remove(element);
    }

    /**
     * Reports whether the collection contains an element.
     *
     * @param element the element to search for.
     * @return true if the element is found, otherwise false
     */
    @Override
    public boolean contains(T element) {
        return table.contains(element);
    }

    /**
     * Returns the number of elements in the collection.
     *
     * @return the number of elements
     */
    @Override
    public int size() {
        return table.size();
    }

    /**
     * Reports whether the collection is empty or not.
     *
     * @return true if the collection is empty, otherwise false
     */
    @Override
    public boolean isEmpty() {
        return table.isEmpty();
    }

    /**
     * Removes all elements from the collection.
     */
    @Override
    public void clear() {
        table.clear();
    }

    /**
     * Returns an element in the collection that matches the
     * input parameter according the equals() method of the
     * parameter.
     *
     * @param element an element to search for
     * @return a matching element
     */
    @Override
    public T get(T element) {
        return (T) table.get(element);
    }

    /**
     * Returns an iterator over the collection.
     *
     * @return an object using the Iterator<T> interface
     */
    @Override
    public Iterator<T> iterator() {
        return table.iterator();
    }
}
